
include config.mk

.PHONY: all
all:
	$(call descend,src,all)

.PHONY: clean
clean:
	$(call descend,src,clean)

.PHONY: install
install:
	$(call descend,src,install)

.PHONY: examples
examples:
	$(call descend,examples,all)

.PHONY: clean-examples
clean-examples:
	$(call descend,examples,clean)

.PHONY: test
test:
	$(call descend,tests,all)

.PHONY: clean-test
clean-test:
	$(call descend,tests,clean)

.PHONY: html
html:
	$(call descend,doc,html)

.PHONY: clean-html
clean-html:
	$(call descend,doc,clean-html)

