
# Where this file resides

root_dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# When version changes, increment this number

VERSION := 1.0.0

# Determine where we are going to install things

prefix := /usr/local
suffix := $(shell echo $(VERSION) | grep -o '^[0-9]*')
bindir := $(prefix)/bin
libdir := $(prefix)/lib
includedir := $(prefix)/include/littleline
datadir := $(prefix)/share/littleline
docdir := $(prefix)/share/doc/littleline
htmldir := $(docdir)
pdfdir := $(docdir)
localstatedir := $(prefix)/var/littleline

# There are so many tools needed...

INSTALL := install
INSTALL_PROGRAM := $(INSTALL)
INSTALL_DATA := $(INSTALL) -m 644

CFLAGS := -g -Werror -Wfatal-errors

override CFLAGS += -std=c99
override CFLAGS += -pedantic
override CFLAGS += -fPIC

# To go down a level $(call descend,directory[,target[,flags]])

descend = make -C $(1) $(2) $(3)

# If the -s option was passed, use these to give a hint of what we are doing

ifneq ($(findstring s,$(filter-out --%,$(MAKEFLAGS))),)
    QUIET_CC =		@echo "CC        $@";
    QUIET_LINK =	@echo "LINK      $@";
    QUIET_AR =		@echo "AR        $@";
    QUIET_GEN =		@echo "GEN       $@";
    QUIET_UNZIP =	@echo "UNZIP     $@";
    QUIET_TEST =	@echo "TEST      $@";
    descend =		@echo "ENTER     `pwd`/$(1)";\
			 make -C $(1) $(2) $(3);\
			 echo "EXIT      `pwd`/$(1)"
endif

%: %.c 

.obj/%.o: %.c | .obj
	$(QUIET_CC)$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

.obj:
	@mkdir $@

%: .obj/%.o
	$(QUIET_LINK)$(CC) $(LDFLAGS) -o $@ $^

%.so:
	$(QUIET_LINK)$(CC) -shared -Wl,-soname,$@.$(suffix) -o $@ $^

%.a:
	$(QUIET_AR)$(AR) rcs $@ $^

